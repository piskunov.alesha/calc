package com.example.calc

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var deleteExpression: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun write(view: View) {
        val button = findViewById<Button>(view.id)
        if ((button.text in "012345789") and (deleteExpression == 1)) {
            expression.text = ""
        }
        val expression: TextView = findViewById(R.id.expression)
        val result = "${expression.text}${button.text}"
        expression.text = result
        deleteExpression = 0
    }

    private fun eval(a: String, op: String): Float {

        if (op == "#") {
            val b = a.toFloat()
            return (b)
        }
        val b: MutableList<String> = a.split(op).toMutableList()
        val c = calc(b, next(op))
        var res = c[0]
        for (i in 1 until c.size) {
            res = done(res, op, c[i])
        }
        return (res)
    }

    @Suppress("UNUSED_PARAMETER")
    fun deleteLastChar(view: View) {
        if (expression.text.toString() == "") return
        val expression: TextView = findViewById(R.id.expression)
        val result = if (deleteExpression == 1)
            "" else
            expression.text.toString().substring(0, expression.text.toString().length - 1)
        expression.text = result
    }

    private fun done(a: Float, op: String, b: Float): Float {
        return when (op) {
            "+" -> a + b
            "-" -> a - b
            "*" -> a * b
            else -> a / b
        }
    }

    fun next(op: String): String {
        return when (op) {
            "+" -> "-"
            "-" -> "*"
            "*" -> "/"
            else -> "#"
        }
    }

    private fun calc(b: MutableList<String>, op: String): MutableList<Float> {
        val res: MutableList<Float> = mutableListOf()
        for (i in 0 until b.size) {
            res.add(eval(b[i], op))
        }
        return (res)
    }

    @Suppress("UNUSED_PARAMETER")
    fun calcValue(view: View) {
        val expression: TextView = findViewById(R.id.expression)
        var text: String = expression.text.toString()
        text = text.replace("÷", "/")
        text = text.replace("×", "*")
        if(text[0] == '-'){
            text = "0$text"
        }
        var result = "Error!"
        try {
            result = eval(text, "+").toString()
        } catch (e: RuntimeException) {
            println("U crazy?")
        }

        expression.text = result
        deleteExpression = 1
    }
}
